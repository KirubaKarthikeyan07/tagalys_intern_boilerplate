$redis = Redis.new(host: "redis", port: 6379, password: "tagalys_redis#2020", timeout: 0.25, reconnect_attempts: 1)

def queue_to_sidekiq(opts, throw_exceptions = false)
  opts.stringify_keys!
  opts['at'] = opts['at'].to_f
  opts['class'] = opts['class'].to_s
  begin
    return Sidekiq::Client.new(ConnectionPool.new { $redis }).push(opts)
  rescue Exception => e
    # alert
    e.log
    raise e if throw_exceptions
  end
end