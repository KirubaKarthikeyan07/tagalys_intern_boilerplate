password = CGI.escape("tagalys_redis#2020")
host = "redis"
port = 6379
url = "redis://:#{password}@#{host}:#{port}"
$abort_operation = false
Sidekiq.configure_server do |config|
  config.average_scheduled_poll_interval = 1
  config.redis = { url: url }
  config.on(:quiet) do
    $abort_operation = true
  end
end
Sidekiq.configure_client do |config|
  config.redis = { url: url }
end