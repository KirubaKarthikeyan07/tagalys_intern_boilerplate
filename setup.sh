echo "INSTALLING DOCKER AND DOCKER COMPOSE"
sudo apt-get update
sudo apt-get install -y apt-transport-https ca-certificates curl gnupg-agent software-properties-common
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu  $(lsb_release -cs)  stable"
sudo apt-get update
echo "ADDING USER TO DOCKER GROUP"
sudo groupadd docker
sudo usermod -aG docker $USER
sudo newgrp docker
echo "PULLING BOILER PLATE"
git clone git@bitbucket.org:KirubaKarthikeyan07/tagalys_intern_boilerplate.git
cd tagalys_intern_boilerplate
echo "BUILDING CONTAINERS"
sudo docker-compose build
echo "BOOTING UP"
docker-compose up