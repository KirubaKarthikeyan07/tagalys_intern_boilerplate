FROM ruby:2.6.4
RUN mkdir /app
WORKDIR /app

# INSTALL NODE AND YARN
RUN curl -sL https://deb.nodesource.com/setup_14.x | bash \
 && apt-get update && apt-get install -y nodejs && rm -rf /var/lib/apt/lists/* \
 && curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add - \
 && echo "deb https://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list \
 && apt-get update && apt-get install -y yarn && rm -rf /var/lib/apt/lists/*

# INSTALL MYSQL CLIENTS
RUN apt-get update && apt-get install -y mariadb-client postgresql-client sqlite3 libsqlite3-dev --no-install-recommends && rm -rf /var/lib/apt/lists/*

# COPYING GEMFILE AND GEMLOCKS
COPY Gemfile /app/Gemfile
COPY Gemfile.lock /app/Gemfile.lock
RUN bundle install

# STARTUP SCRIPT
COPY entrypoint.sh /usr/bin/
RUN chmod +x /usr/bin/entrypoint.sh
ENTRYPOINT ["entrypoint.sh"]