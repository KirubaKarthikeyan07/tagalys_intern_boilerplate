Rails.application.routes.draw do
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  match 'queue_test_worker', to: 'application#queue_test_worker', via: :get
end
