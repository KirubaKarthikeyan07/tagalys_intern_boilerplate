# require 'typhoeus'
# require 'typhoeus/adapters/faraday'
require 'elasticsearch'

$es = Elasticsearch::Client.new({
  host: 'http://172.17.0.1:9200',
  retry_on_failure: true,
  transport_options: { request: { open_timeout: 1 } },
  log: true
})
