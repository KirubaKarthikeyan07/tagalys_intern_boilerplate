class ApplicationController < ActionController::Base
  def queue_test_worker
    queue_to_sidekiq({
      'queue' => 'test',
      'class' => 'TestWorker',
      'args' => [],
      'at' => Time.now
    })
    render json: {
      queued: true
    }
  end
end
